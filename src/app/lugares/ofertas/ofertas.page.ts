import { LugaresService } from './../lugares.service';
import { Lugar } from './../lugar.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.page.html',
  styleUrls: ['./ofertas.page.scss'],
})
export class OfertasPage implements OnInit {

  ofertas: Lugar[];

  constructor(private offerService: LugaresService){ }

  ngOnInit() {
  }
  this.ofertas = this.offersService.lugares;

}
